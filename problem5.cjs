let problem4 = require('./problem4.cjs')
function problem5(inventory, year){
    let carsBeforeYear = [];
    const carYear = problem4(inventory);
    for(let index = 0; index < carYear.length; index++){
        if(carYear[index] < year){
            carsBeforeYear.push(carYear[index]);
        }
    }
    return carsBeforeYear;
}
module.exports = problem5;