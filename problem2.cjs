function problem2(inventory){

    let lastCar = [];
    if(inventory != null && inventory.length !=0 && Array.isArray(inventory)){
        const lastIndex = inventory.length - 1;
        lastCar.push(inventory[lastIndex]);
    }
    return lastCar;
}
module.exports = problem2;