function problem1(inventory, searchId){
    let foundCar = [];
    if(inventory != null && searchId != null && Array.isArray(inventory)){
        for(let index = 0; index < inventory.length; index++){
            if(inventory[index].id == searchId){
                foundCar.push(inventory[index]);
                break;
            }
        }
    }
    return foundCar;
}
module.exports = problem1;