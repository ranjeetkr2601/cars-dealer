let problem4 = require('./problem4.cjs')

function testProblem4(inventory){
    const carYear = problem4(inventory);
    console.log(carYear);
}

module.exports = testProblem4;