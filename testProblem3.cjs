let problem3 = require('./problem3.cjs')

function testProblem3(inventory){
    const sortedCarModels = problem3(inventory);
    console.log(sortedCarModels);
}

module.exports = testProblem3;