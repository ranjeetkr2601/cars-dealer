let problem1 = require('./problem1.cjs')

const searchId = 33;
function testProblem1(inventory){
    const foundCar = problem1(inventory, searchId);
    if(foundCar.length != 0){
        console.log(`Car ${foundCar[0].id} is a ${foundCar[0].car_year} ${foundCar[0].car_make} ${foundCar[0].car_model}`) 
    }
    else{
        console.log(`No car found with ID ${searchId}`);
    }
}

module.exports = testProblem1;