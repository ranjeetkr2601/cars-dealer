function problem3(inventory){
    const carModels = [];
    if(inventory != null && inventory.length != 0 && Array.isArray(inventory)){
        for(let index = 0; index < inventory.length; index++){
            carModels.push(inventory[index].car_model);
        }
        carModels.sort();
    }
    return carModels;
}
module.exports = problem3;