
function problem6(inventory){
    const selectedCars = [];
    if(inventory != null && inventory.length != 0 && Array.isArray(inventory)){
        for(let index = 0; index < inventory.length; index++){
            let carMake = inventory[index].car_make;
            if(carMake.toLowerCase() == "bmw" || carMake.toLowerCase() == "audi"){
                selectedCars.push(inventory[index]);
            }
        }
    }
    return selectedCars;
}
module.exports = problem6;