let problem5 = require('./problem5.cjs')

const year = 2000;
function testProblem5(inventory){
    const carsBeforeYear = problem5(inventory, year);
    if(carsBeforeYear.length != 0){
        console.log(carsBeforeYear.length);
    }
    else{
        console.log(0);
    }
}

module.exports = testProblem5;