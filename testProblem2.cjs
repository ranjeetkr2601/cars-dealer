let problem2 = require('./problem2.cjs')

function testProblem2(inventory){
    const lastCar = problem2(inventory);
    if(lastCar.length != 0){
        console.log(`Last car is a ${lastCar[0].car_make} ${lastCar[0].car_model}`)
    }
    else{
        console.log("Inventory empty or invalid type");
    }
}

module.exports = testProblem2;